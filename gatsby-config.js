// Initialize dotenv
require('dotenv').config({
  path: `.env.${process.env}`, // or '.env'
});

const lessToJson = require('less-to-json');

module.exports = {
  siteMetadata: {
    title: 'SHELBY',
    description: 'Pagina Oficial de Multiservicios Automotriz Shelby',
    author: 'castleCode',
    lang: 'es-ES',
    siteType: 'website',
    siteUrl: 'https://multiserviciosshelby.com.co/',
  },
  flags: {
    PRESERVE_FILE_DOWNLOAD_CACHE: true,
    PRESERVE_WEBPACK_CACHE: true,
    // FAST_DEV: true,
    // DEV_SSR: true,
    // QUERY_ON_DEMAND: true,
    // FAST_REFRESH: true,
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-offline',
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    'gatsby-plugin-fontawesome-css',
    'gatsby-plugin-htaccess',
    {
      resolve: 'gatsby-plugin-antd',
      options: {
        style: true,
      },
    },

    {
      resolve: 'gatsby-plugin-htaccess',
      options: {
        RewriteBase: '/',
        https: true,
        www: true,
        SymLinksIfOwnerMatch: true,
        host: 'www.multiserviciosshelby.com.co', // if 'www' is set to 'false', be sure to also remove it here!
        ErrorDocument: `
          ErrorDocument 404 /404.html
        `,
      },
    },

    {
      resolve: 'gatsby-plugin-less',
      options: {
        lessOptions: {
          javascriptEnabled: true,
          modifyVars: lessToJson('src/assets/theme/vars.less'),
        },
      },
    },

    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/assets/images`,
      },
    },

    {
      resolve: 'gatsby-plugin-mailgo',
      options: {
        mailgoConfig: {
          dark: true,
          lang: 'es',
          es: {
            open_in_: 'abrir con ',
            bcc_: 'cco ',
            subject_: 'asunto ',
            body_: 'cuerpo ',
            call: 'llamar',
            open: 'abrir',
            _default: ' predefinido',
            _as_default: ' por defecto',
            copy: 'copiar',
            copied: 'copiado',
          },
        },
      },
    },

    {
      resolve: 'gatsby-plugin-eslint',
      options: {
        test: /\.js$|\.jsx$/,
        exclude: /(node_modules|.cache|public)/,
        stages: ['develop'],
        options: {
          emitWarning: true,
          failOnError: false,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-svgr-loader',
      options: {
        rule: {
          include: /\.inline\.svg$/,
        },
      },
    },

    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'Multiservicos Shelby Automotriz',
        short_name: 'Shelby',
        start_url: '/',
        background_color: '#ffff',
        theme_color: '#061629',
        display: 'minimal-ui',
        icon: 'src/assets/images/icon.png', // This path is relative to the root of the site.
      },
    },
  ],
};
