import { useState } from 'react';

export default function useLocalStorage() {
  const IS_SERVER = typeof window === 'undefined';
  const [start, setStart] = useState(IS_SERVER ? null : () => window.localStorage.getItem('rate'));

  function saveToLocalStorage(rate) {
    window.localStorage.setItem('rate', rate);
  }

  return {
    saveToLocalStorage,
    start,
    setStart,
  };
}
