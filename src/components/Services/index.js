import React from 'react';

import { Card, Row, Col, Avatar, Typography } from 'antd';

import { servicesData } from '../../assets/data';

import Image from '../Images/image';

import cobraLogo from '../../assets/images/avatarCobra.png';

import './Services.less';

const { Meta } = Card;
const { Paragraph } = Typography;

const AvatarShelby = () => <Avatar size={32} src={cobraLogo} />;

const imgService = (img, label) => (
  <div
    style={{
      maxWidth: '100%',
    }}
  >
    <Image filename={img} alt={label} />
  </div>
);

const handleDescription = (text) => (
  <Paragraph
    ellipsis={{
      rows: 1,
      expandable: true,
      symbol: 'Más',
    }}
  >
    {text}
  </Paragraph>
);

const Services = () => (
  <div className="section">
    <div className="overlay" />
    <div className="Services">
      <Row gutter={[16, 16]} className="Services-wraper">
        {servicesData.map(({ id, label, description, image }) => (
          <Col xs={24} sm={24} md={8} lg={8} key={id}>
            <Card
              hoverable
              title={label}
              cover={imgService(image, label)}
              description={description}
            >
              <Meta avatar={<AvatarShelby />} description={handleDescription(description)} />
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  </div>
);

export default Services;
