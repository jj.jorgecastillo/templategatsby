/* eslint-disable object-curly-newline */
import React from 'react';

import { navigate } from 'gatsby';

import { Form, Input, Row, Col, Button, notification, Typography, Select, Grid } from 'antd';

import {
  FrownTwoTone,
  MailTwoTone,
  PhoneTwoTone,
  SendOutlined,
  SmileOutlined,
} from '@ant-design/icons';
import { servicesData } from '../../assets/data/index';
import Mouse from '../Mouse';

import './FormContact.less';

const validateMessages = {
  required: '${label} Es requerido!', // eslint-disable-line
  types: {
    email: '${label} No es un correo válido!', // eslint-disable-line
    number: '${label} Solo se permiten números!', // eslint-disable-line
  },
};

const { TextArea } = Input;
const { Title } = Typography;
const { Option } = Select;
const { useBreakpoint } = Grid;

const FormContact = () => {
  const [form] = Form.useForm();
  const breakpoints = useBreakpoint();

  const onFinish = ({ nombre, email, telefono, mensaje, carro, servicio }) => {
    const formData = new FormData();

    formData.append('nombre', nombre);
    formData.append('email', email);
    formData.append('telefono', telefono);
    formData.append('servicio', servicio);
    formData.append('carro', carro);
    formData.append('mensaje', mensaje);
    formData.append('_template', 'table');
    formData.append('_captcha', false);
    formData.append('_cc', 'jj.jorgecastillo@gmail.com');

    fetch('https://formsubmit.co/tallershelby@gmail.com', {
      method: 'POST',
      mode: 'no-cors',
      headers: {
        ContentType: 'application/multipart/form-data',
      },
      body: formData,
    })
      .then(() => {
        notification.open({
          type: 'success',
          icon: (
            <SmileOutlined
              style={{
                color: '#108ee9',
              }}
            />
          ),
          message: 'Mensaje enviado con éxito',
          duration: 5,
          description: `${nombre} Pronto nos comunicaremos contigo.`,
        });
        form.resetFields();
        navigate('/resultpage');
      })
      .catch(() => {
        notification.open({
          type: 'error',
          icon: <FrownTwoTone twoToneColor="#eb2f96" />,
          message: 'Mensaje No enviado ',
          duration: 5,
          description: `${nombre} Ocurrio un error, intenta nuevamente.`,
        });
      });
  };

  return (
    <div className="section">
      <div className="overlay" />
      <Mouse black={false} />
      <div className="FormContact">
        <Row gutter={[16, 16]}>
          <Form
            form={form}
            onFinish={onFinish}
            validateMessages={validateMessages}
            layout="vertical"
            className="FormContact-wrapperForm"
          >
            <Col xs={24}>
              <Row gutter={[8, 8]}>
                <Col xs={24}>
                  <Title level={5}>Recibe una atención personalizada.</Title>
                </Col>
                <Col xs={12} sm={12}>
                  <Form.Item name="nombre" label="Nombre" rules={[{ required: true }]}>
                    <Input />
                  </Form.Item>
                </Col>
                <Col xs={12} sm={12}>
                  <Form.Item name="email" label="Email" rules={[{ required: true, type: 'email' }]}>
                    <Input />
                  </Form.Item>
                </Col>
                <Col xs={12} sm={12}>
                  <Form.Item name="telefono" label="Teléfono" rules={[{ required: true }]}>
                    <Input />
                  </Form.Item>
                </Col>
                <Col xs={12} sm={12}>
                  <Form.Item name="servicio" label="Servicio" rules={[{ required: true }]}>
                    <Select>
                      {servicesData.map(({ id, label }) => (
                        <Option title={label} key={id} value={label}>
                          {label}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col xs={24}>
                  <Form.Item name="mensaje" label="Mensaje" rules={[{ required: true }]}>
                    <TextArea
                      className="field"
                      autoSize={{
                        minRows: 2,
                        maxRows: 3,
                      }}
                      placeholder="Escribe tu consulta pronto te responderemos"
                      showCount
                      maxLength={200}
                    />
                  </Form.Item>
                </Col>
                <Col
                  xs={24}
                  style={{
                    marginTop: '4px',
                    display: 'flex',
                    justifyContent: 'center',
                    zIndex: 100,
                  }}
                >
                  <Form.Item>
                    <Button
                      icon={<SendOutlined />}
                      type="default"
                      size="large"
                      shape="round"
                      htmlType="submit"
                      style={{
                        background: '#ffdf00',
                        color: '#061629',
                      }}
                    >
                      Enviar
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          </Form>

          <Col xs={24}>
            <Row gutter={[8, 8]}>
              <Col xs={24}>
                <div className={breakpoints.xs ? 'SocialMedia-title-small' : 'SocialMedia-title'}>
                  Comunicate
                </div>
              </Col>

              <Col xs={12}>
                <Button type="text" size="large" icon={<PhoneTwoTone />}>
                  <a href="tel:+573144242856">
                    <strong className="IconLink" style={{ color: '#ffdf00' }}>
                      LLAMADA
                    </strong>
                  </a>
                </Button>
              </Col>
              <Col xs={12}>
                <Button type="text" size="large" icon={<MailTwoTone />}>
                  <a href="mailto:tallershelby@gmail.com">
                    <strong className="IconLink" style={{ color: '#ffdf00' }}>
                      ESCRIBIR
                    </strong>
                  </a>
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default FormContact;
