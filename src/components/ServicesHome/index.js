import React from 'react';
import { Typography } from 'antd';

import Mouse from '../Mouse';

import './ServicesHome.less';
import ImageShelby from '../Images/ImageShelby';

const { Title } = Typography;

const ServicesHome = () => (
  <section className="section">
    <div className="layerShelby">
      <ImageShelby />
    </div>
    <div className="ServicesHome fullpage-wrapper">
      <Mouse black={false} />
      <div className="ServicesHome-galery">
        <div className="overlay">
          <div className="slide imageServiceBackground" />
        </div>
      </div>
    </div>

    <div className="layerSubTitle">
      <Title level={4}>Tu auto siempre como nuevo.</Title>
    </div>
  </section>
);

export default ServicesHome;
