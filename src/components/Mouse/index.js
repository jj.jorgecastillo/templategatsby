import React from 'react';
import mouseWhite from '../../assets/images/scroll_icon_white.png';
import mouseBlack from '../../assets/images/scroll_icon.png';

import './Mouse.less';

const Mouse = ({ black = true }) => (
  <div className="section-footer-wrapper animated fadeIn">
    <div className="scroll-animation">
      <img src={black ? mouseBlack : mouseWhite} alt="mouse" />
    </div>
  </div>
);

export default Mouse;
