import React from 'react';

import { Link } from 'gatsby';

import { Menu, Divider } from 'antd';

import { menuData } from '../../assets/data';

import './MenuShelby.less';

const { Item } = Menu;

const MenuShelby = ({ mode, divider = false }) => {
  const menuItem = ({ key, icon: Icon, text }) => (
    <>
      <Item key={key}>
        <Icon twoToneColor="#1cb5e0" />
        <Link to={`/${key}`}>{text}</Link>
      </Item>
      {divider && <Divider type={mode} />}
    </>
  );

  return (
    <>
      <Menu theme="dark" mode={mode} className="MenuShelby">
        {menuData.map((item) => menuItem(item))}
      </Menu>
    </>
  );
};

export default MenuShelby;
