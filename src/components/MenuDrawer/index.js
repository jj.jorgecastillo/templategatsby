import React, { useState } from 'react';

import { Drawer, Button, Avatar } from 'antd';
import { MenuOutlined } from '@ant-design/icons';

import shelbyTransparente from '../../assets/images/shelbyTransparente.png';

import MenuShelby from '../MenuShelby';

import './MenuDrawer.less';

const MenuDrawer = () => {
  const [showMenu, setShowMenu] = useState(false);

  const handleToggleMenu = () => setShowMenu(!showMenu);

  const title = (image) => (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      <Avatar shape="square" size={100} src={image} />
    </div>
  );

  return (
    <div className="MenuDrawer">
      <Button className="Header-toggler" icon={<MenuOutlined />} onClick={handleToggleMenu} />
      {showMenu && (
        <Drawer
          placement="left"
          width={250}
          visible
          onClose={handleToggleMenu}
          title={title(shelbyTransparente)}
          closable={false}
          className="MenuDrawer-menu"
        >
          <MenuShelby mode="vertical" divider visible={false} />
        </Drawer>
      )}
    </div>
  );
};

export default MenuDrawer;
