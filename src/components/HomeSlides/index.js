import React from 'react';

import { Typography } from 'antd';

import { sideData } from '../../assets/data';
import './HomeSlides.less';
import Mouse from '../Mouse';

const { Title } = Typography;

const HomeSlides = () => (
  <section className="section">
    <div className="overlay" />
    <div className="HomeSlides fullpage-wrapper">
      <div className="HomeSlides-galery">
        {sideData.map(({ key, label }) => (
          <div key={key} className={`slide sd${key}`}>
            <Title level={2}>{label}</Title>
          </div>
        ))}
      </div>
    </div>
    <Mouse black={false} />
  </section>
);

export default HomeSlides;
