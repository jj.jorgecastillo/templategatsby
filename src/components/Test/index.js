import React from 'react';
import NewCard from '../NewCard';
import Services from '../Services';
import ServicesHomeVideo from '../ServicesHomeVideo';
import SocialMedia from '../SocialMedia';

import './Test.less';

const Test = () => (
  <>
    <div id="header1" />
    <div id="wrapper">
      <ul id="menu">
        <li data-menuanchor="firstPage1" className="active">
          <a href="#firstPage1">1a</a>
        </li>
        <li data-menuanchor="firstPage2" className="">
          <a href="#firstPage2">1b</a>
        </li>
        <li data-menuanchor="firstPage3" className="">
          <a href="#firstPage3">1c</a>
        </li>
        <li data-menuanchor="secondPage">
          <a href="#secondPage">2</a>
        </li>
        <li data-menuanchor="3rdPage">
          <a href="#3rdPage">3</a>
        </li>
      </ul>
      <div id="fullpage">
        <div data-percentage="100" className="section fullpage-wrapper" id="section0">
          <ServicesHomeVideo />
        </div>
        <div data-percentage="80" className="section " id="section0">
          <h2>Section set with data-percentage=80</h2>
        </div>
        <div data-percentage="70" className="section " id="section0">
          <h2>Section set with data-percentage=70</h2>
        </div>
        <div className="section" id="section1">
          <div className="slide active">
            <div className="intro">
              <h1>Create Sliders</h1>
              <p>
                Not only vertical scrolling but also horizontal scrolling. With fullPage.js you will
                be able to add horizontal sliders in the most simple way ever.
              </p>
            </div>
          </div>
          <div className="slide">
            <div className="intro">
              <h1>Simple</h1>
              <p>Easy to use. Configurable and customizable.</p>
            </div>
          </div>
          <div className="slide">
            <div className="intro">
              <h1>Cool</h1>
              <p>It just looks cool. Impress everybody with a simple and modern web design!</p>
            </div>
          </div>
          <div className="slide">
            <div className="intro">
              <h1>Compatible</h1>
              <p>
                Working in modern and old browsers too! IE 8 users have the fault of using that
                horrible browser! Lets give them a chance to see your site in a proper way!
              </p>
            </div>
          </div>
        </div>
        <div className="section" id="section2">
          <div className="intro">
            <h1>Easy to use plugin</h1>
            <p>HTML markup example to define 4 sections.</p>
          </div>
        </div>
        <div className="section" id="section3">
          <div className="intro">
            <h1
              style={{
                padding: '0 10%',
              }}
            >
              <NewCard />
            </h1>
          </div>
        </div>
        <div className="fp-auto-height" id="section4">
          <SocialMedia />
        </div>
      </div>
    </div>
  </>
);

export default Test;
