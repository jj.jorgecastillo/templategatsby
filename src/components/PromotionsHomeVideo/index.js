import React from 'react';
import { Typography } from 'antd';

import poster from '../../assets/images/PosterPromos.jpg';
import video from '../../assets/video/happy1.mp4';
import video2 from '../../assets/video/happy1.webm';

import Mouse from '../Mouse';

import './PromotionsHomeVideo.less';
import ImageShelby from '../Images/ImageShelby';

const { Title } = Typography;

const PromotionsHomeVideo = () => (
  <div className="section fullpage-wrapper">
    <div className="layerShelby">
      <ImageShelby />
    </div>
    <video poster={poster} className="myVideoPromotions" loop muted data-autoplay data-keepplaying>
      <source src={video} type="video/mp4" />
      <source src={video2} type="video/webm" />
    </video>
    <div className="layerSubTitle">
      <Title level={4}>Promociones alcance de tu mano.</Title>
    </div>
    <Mouse black={false} />
  </div>
);

export default PromotionsHomeVideo;
