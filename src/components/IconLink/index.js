import React from 'react';

import { Row, Col, Grid } from 'antd';

import './IconLink.less';

const { useBreakpoint } = Grid;

const IconLink = ({ icon, text, href, color = '', xs = 6, md = 6 }) => {
  const breakpoints = useBreakpoint();
  return (
    <Col xs={xs} md={md}>
      <Row gutter={[16, 16]}>
        <Col xs={24} md={24} className="IconLink">
          <a href={href} target="_blank" rel="noreferrer">
            <div
              style={{
                maxWidth: '100%',
                width: breakpoints ? 40 : 200,
                zIndex: 100,
              }}
            >
              {icon}

              <strong
                style={{
                  margin: '0 auto',
                  fontSize: breakpoints ? 14 : 20,
                  color: '#FFF',
                  background: color,
                  borderRadius: 8,
                  padding: 4,
                }}
              >
                {text}
              </strong>
            </div>
          </a>
        </Col>
      </Row>
    </Col>
  );
};

export default IconLink;
