import React from 'react';
import { Typography } from 'antd';

import poster from '../../assets/images/PosterService.jpg';
import video from '../../assets/video/services4in1.mp4';
import video2 from '../../assets/video/services.mp4';

import Mouse from '../Mouse';

import './ServicesHomeVideo.less';
import ImageShelby from '../Images/ImageShelby';

const { Title } = Typography;

const ServicesHomeVideo = () => (
  <div className="section fullpage-wrapper">
    <div className="layerShelby">
      <ImageShelby />
    </div>
    <video poster={poster} className="myVideoService" loop muted data-autoplay data-keepplaying>
      <source src={video} type="video/mp4" />
      <source src={video2} type="video/webm" />
    </video>
    <div className="layerSubTitleService">
      <Title level={4}>Todos los servicios en un solo lugar.</Title>
    </div>
    <Mouse black={false} />
  </div>
);

export default ServicesHomeVideo;
