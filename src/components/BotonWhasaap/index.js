import React from 'react';

import { Grid } from 'antd';

import Whatsapp from '../../assets/svg/whatsapp.inline.svg';
import Messenger from '../../assets/svg/messenger.inline.svg';

import './BotonWhasaap.less';

const { useBreakpoint } = Grid;

const BotonWhasaap = () => {
  const breakpoints = useBreakpoint();

  return (
    <div
      className="BotonWhasaap"
      style={{
        width: breakpoints.xs ? '8%' : '3%',
      }}
    >
      <a href="https://wa.link/d8u2uk" target="_blank" rel="noreferrer">
        <Whatsapp />
      </a>
      <a href="https://m.me/tallershelby" target="_blank" rel="noreferrer">
        <Messenger />
      </a>
    </div>
  );
};

export default BotonWhasaap;
