import React from 'react';

import { Card, Grid, Tabs } from 'antd';

import { servicesData } from '../../assets/data';

import './NewCard.less';

const { Meta } = Card;

const { useBreakpoint } = Grid;

const { TabPane } = Tabs;

const NewCard = () => {
  const breakpoints = useBreakpoint();

  function validationSize(size) {
    const { xs, sm, md } = size;

    if (md) {
      return '100%';
    }

    if (sm) {
      return '100%';
    }

    if (xs) {
      return '100%';
    }

    return '100%';
  }

  function validationSizeTab(size) {
    const { xs, sm, md } = size;

    if (md) {
      return 'left';
    }

    if (sm) {
      return 'top';
    }

    if (xs) {
      return 'top';
    }

    return 'left';
  }

  const imgService = (img, label) => <img alt={label} src={img} />;

  return (
    <div className="section">
      <div className="overlay NewCard" />
      <Tabs
        defaultActiveKey="1"
        tabPosition={validationSizeTab(breakpoints)}
        className="NewCard-wrapper"
        type="card"
        size="small"
      >
        {servicesData.map(({ id, label, description, image }) => (
          <TabPane tab={label} key={id}>
            <Card
              style={{
                width: validationSize(breakpoints),
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column',
                alignItems: 'center',
                margin: '0 auto',
              }}
              cover={imgService(image, label)}
            >
              <Meta description={description} />
            </Card>
          </TabPane>
        ))}
      </Tabs>
    </div>
  );
};

export default NewCard;
