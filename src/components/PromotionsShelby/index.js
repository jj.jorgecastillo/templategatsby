/* eslint-disable object-curly-newline */
import React from 'react';

import { Avatar, Card, Row, Col, Statistic, Tag } from 'antd';

import { TagsTwoTone, PercentageOutlined, DollarCircleFilled, CarTwoTone } from '@ant-design/icons';

import { PromotionsData } from '../../assets/data';

import './Promotions.less';

const { Meta } = Card;

const handlePromo = ({ src, alt }) => (
  <div>
    <img src={src} alt={alt} />
  </div>
);

// { price = 0, discount = 0 }
// const survey = (price * discount) / 100;
// const total = price - survey;

const promoPrice = () => (
  // <Tooltip title={`Ahora pagas ${total} Mil pesos`} placement="topLeft" color="green">
  <Tag color="green">
    <Statistic
      title={<small style={{ color: 'red', fontWeight: 700, fontSize: '1rem' }}>OFERTA</small>}
      // value={discount}
      valueStyle={{ color: 'red', fontWeight: 700, fontSize: '1rem' }}
      suffix={<PercentageOutlined />}
    />
    <strong>DESCUENTO</strong>
  </Tag>
  // </Tooltip>
);

const itemsDescription = (dataItem = []) => (
  <>
    {dataItem.map(({ idPromo, item }) => (
      <div
        key={idPromo}
        style={{
          listStyleType: 'none',
          margin: 0,
          padding: 0,
          textAlign: 'left',
        }}
      >
        <TagsTwoTone />
        <small style={{ marginLeft: 4 }}>{item}</small>
      </div>
    ))}
  </>
);

const handlePrice = ({ price }) => (
  <>
    {price > 0 ? (
      <div>
        <Statistic
          value={price}
          valueStyle={{ color: '#52c41a', fontWeight: 900, fontSize: '2rem' }}
          suffix={<DollarCircleFilled />}
        />
        <small style={{ color: '#52c41a', fontWeight: 900, fontSize: '1rem' }}>Mil Pesos</small>
      </div>
    ) : (
      <strong>
        <Avatar
          style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}
          size="large"
          icon={<CarTwoTone twoToneColor="#52c41a" style={{ fontSize: '24px' }} />}
        />
        <br />
        <small style={{ color: '#f56a00' }}>Según Vehículo </small>
      </strong>
    )}
  </>
);

const Promos = () => (
  <section className="section fullpage-wrapper">
    <div className="overlay" />
    <div className="Promotions">
      <div className="Promotions-wraper">
        <Row gutter={[16, 32]}>
          {PromotionsData.map(({ id, src, title, price = 0, discount = 0, note, description }) => (
            <Col xs={24} sm={12} md={12} lg={8} key={id}>
              <Card
                hoverable
                title={title}
                cover={handlePromo({ src, alt: title })}
                actions={[
                  note && (
                    <small key="promo">
                      <Tag color="magenta">NOTA: </Tag>
                      {note}
                    </small>
                  ),
                ]}
                extra={handlePrice({ price })}
              >
                <Meta
                  title={<Tag color="gold">ESTA PROMOCIÓN INCLUYE</Tag>}
                  avatar={discount > 0 && promoPrice({ price, discount })}
                  description={itemsDescription(description)}
                />
              </Card>
            </Col>
          ))}
        </Row>
      </div>
    </div>
  </section>
);

export default Promos;
