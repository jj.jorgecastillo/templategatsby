import React from 'react';

import { Link } from 'gatsby';

import { Result, Button, Typography } from 'antd';

import './ResultMessage.less';

const { Title } = Typography;

function ResultMessage() {
  return (
    <Result
      className="ResultMessage-title"
      status="success"
      title={<Title level={2}>Recibimos tu email</Title>}
      subTitle="Gracias por contactarnos, pronto te respondemos."
      extra={[
        <Button type="primary" key="services">
          <Link to="/services">Ver servicios</Link>
        </Button>,
        <Button key="promotions">
          <Link to="/promotions">Ver promociones</Link>
        </Button>,
      ]}
    />
  );
}

export default ResultMessage;
