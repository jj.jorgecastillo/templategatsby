import React from 'react';

import { Link } from 'gatsby';

import { ConfigProvider, Layout, Grid } from 'antd';

import locale from 'antd/lib/locale/es_ES';

import MenuShelby from '../MenuShelby';

import MenuDrawer from '../MenuDrawer';

import '../../assets/theme/app.less';

import BotonWhasaap from '../BotonWhasaap';

const { Header, Content } = Layout;
const { useBreakpoint } = Grid;

const Container = ({ children }) => {
  const breakpoints = useBreakpoint();
  return (
    <ConfigProvider locale={locale}>
      <Layout className="Layout">
        <Header
          style={{
            position: 'sticky',
            top: 0,
            zIndex: 100,
            width: '100%',
            height: 'auto',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <div>
            <Link to="/" className="mark">
              SHELBY
            </Link>
          </div>
          {breakpoints.lg ? <MenuShelby /> : <MenuDrawer />}
        </Header>
        <Content>{children}</Content>
        <BotonWhasaap />
      </Layout>
    </ConfigProvider>
  );
};

export default Container;
