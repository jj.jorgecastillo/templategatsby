import React, { useState } from 'react';

import { Rate } from 'antd';

import useLocalStorage from '../../Hooks/useLocalStorage';

import './Rater.less';

const desc = ['Malo', 'Normal', 'Bueno', 'Eficiente', 'Maravilloso'];

const Rater = () => {
  const { saveToLocalStorage, start, setStart } = useLocalStorage();
  const [value, setValue] = useState(start);
  const handleChange = (rate) => {
    saveToLocalStorage(rate);
    setStart(rate);
    setValue(rate);

    if (rate) {
      const features = 'center=yes; width=700; height=600';
      const href = 'http://search.google.com/local/writereview?placeid=ChIJhx7itmdHZo4RaVVriHSinIE';
      window.open(href, 'name', features, false);
    }
  };

  return (
    <span>
      <Rate tooltips={desc} onChange={handleChange} value={value} />
      {value ? <span className="ant-rate-text">{desc[value - 1]}</span> : ''}
    </span>
  );
};

export default Rater;
