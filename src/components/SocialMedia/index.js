/* eslint-disable object-curly-newline */
import React from 'react';

import { Layout, Grid, Row, Col, Tag } from 'antd';

import Rater from '../Rater';

import Twitter from '../../assets/svg/twitter.inline.svg';
import Facebook from '../../assets/svg/facebook.inline.svg';
import Instagram from '../../assets/svg/instagram.inline.svg';
import Youtube from '../../assets/svg/youtube.inline.svg';
import IconLink from '../IconLink';

import './SocialMedia.less';

const { Footer } = Layout;
const { useBreakpoint } = Grid;

const socialContent = (
  <>
    <IconLink
      icon={<Instagram />}
      text="Instagram"
      href="https://www.instagram.com/tallershelbyoficial/"
      color="#FFA500"
    />
    <IconLink
      icon={<Facebook />}
      text="Facebook"
      href="https://www.facebook.com/tallershelby/"
      color="#3C5A99"
    />

    <IconLink
      icon={<Twitter />}
      text="Twitter"
      href="https://twitter.com/tallershelby"
      color="#439CD6"
    />
    <IconLink
      icon={<Youtube />}
      text="Youtube"
      href="https://www.youtube.com/channel/UCj_wz2UWe5lCVoNVrO6fzqg?view_as=subscriber"
      color="#CE1312"
    />
  </>
);

const SocialMedia = () => {
  const breakpoints = useBreakpoint();
  return (
    <Footer className="section fullpage-wrapper SocialMedia">
      <div className="overlay" />
      <Row gutter={[4, 8]} className="SocialMedia-wrapper">
        <Col xs={24} md={12} style={{ display: 'grid', placeItems: 'center' }}>
          <Row gutter={[4, 16]}>
            <Col xs={24}>
              <a href="https://codecastle.cc/" target="_blank" rel="noreferrer">
                <strong>
                  <Tag style={{ fontSize: '1rem' }} color="blue">
                    Made with ❤️ by CodeCastle
                  </Tag>
                </strong>
              </a>
            </Col>
            <Col xs={24}>
              <div className={breakpoints.xs ? 'SocialMedia-title-small' : 'SocialMedia-title'}>
                <strong>
                  <a href="https://g.page/taller-shelby?gm" target="_blank" rel="noreferrer">
                    Deja tu reseña
                  </a>
                </strong>
              </div>
              <div>
                <strong style={{ color: '#fafafa' }}>¿Que tal nuestro servicio?</strong>
              </div>
              <div>
                <Rater />
              </div>
            </Col>

            <div className="SocialMedia-title-small">
              Usamos TallerAlpha para la recepcion del vehiculo.
            </div>
            <div className="cell">
              <Col className="video">
                <iframe
                  className="youtube_video"
                  src="https://www.youtube.com/embed/fm-469rcDtQ?controls=1&rel=0"
                  title="Usamos taller alpha"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  frameBorder="0"
                  allowFullScreen
                  aria-hidden="false"
                  loading="lazy"
                />
              </Col>
            </div>
          </Row>
        </Col>
        <Col xs={24} md={12}>
          <Row gutter={[4, 16]}>
            <div className="SocialMedia-title-small">
              Siguenos y enterate de nuevas ofertas y promociones
            </div>
            {socialContent}
            <Col xs={24}>
              <iframe
                className="iframe"
                title="Calle Septima Cl. 7 #3-213"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.3340852781907!2d-72.48062618522142!3d7.860062494336915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x819ca274886b5569!2sTaller%20Shelby!5e0!3m2!1ses!2sco!4v1616342630068!5m2!1ses!2sco&zoom=18&maptype=satellite"
                width="100%"
                height="auto"
                frameBorder="0"
                allowFullScreen
                aria-hidden="false"
                loading="lazy"
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </Footer>
  );
};

export default SocialMedia;
