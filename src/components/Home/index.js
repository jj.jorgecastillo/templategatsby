import React from 'react';

import { Typography } from 'antd';

import ImageShelby from '../Images/ImageShelby';
import video from '../../assets/video/intro.mp4';
import video2 from '../../assets/video/intro.webm';
import Mouse from '../Mouse';

import './Home.less';

const { Title } = Typography;

const Home = () => (
  <div className="section fullpage-wrapper">
    <video className="myVideo" loop muted data-autoplay data-keepplaying>
      <source src={video} type="video/mp4" />
      <source src={video2} type="video/webm" />
    </video>
    <div className="layerShelby">
      <ImageShelby />
    </div>

    <div className="layerTitle">
      <Title level={3}>Bienvenidos</Title>
    </div>
    <div className="layerSubTitle">
      <Title level={4}>Abrimos todos los dias 6AM - 10PM</Title>
    </div>
    <Mouse black={false} />
  </div>
);

export default Home;
