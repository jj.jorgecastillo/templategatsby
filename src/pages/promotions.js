import React from 'react';
import ReactFullpage from '@fullpage/react-fullpage';

import SEO from '../components/Seo/index';
import Container from '../components/Container';

import SocialMedia from '../components/SocialMedia';
import PromotionsShelby from '../components/PromotionsShelby';
import PromotionsHomeVideo from '../components/PromotionsHomeVideo';

const Promotions = () => (
  <Container>
    <SEO title="Promociones" description="Promociones del MultiservicosShelby" />

    <ReactFullpage
      lockAnchors={false}
      navigationPosition="right"
      slidesNavigation
      navigation
      slidesNavPosition="top"
      autoScrolling
      scrollBar={false}
      easing="easeInOutCubic"
      loopHorizontal
      continuousHorizontal={false}
      scrollHorizontally={false}
      interlockedSlides={false}
      dragAndMove={false}
      offsetSections={false}
      resetSliders={false}
      fadingEffect="sections"
      scrollOverflow
      touchSensitivity={15}
      bigSectionsDestination="null"
      keyboardScrolling
      animateAnchor
      recordHistory
      controlArrows={false}
      verticalCentered
      paddingBottom={4}
      fixedElements=".footer"
      responsiveWidth={0}
      responsiveHeight={0}
      responsiveSlides
      continuousVertical={false}
      paddingTop={4}
      scrollingSpeed={1500}
      render={() => (
        <div className="Fullpage">
          <PromotionsHomeVideo />
          <PromotionsShelby />
          <SocialMedia />
        </div>
      )}
    />
  </Container>
);

export default Promotions;
