import React from 'react';

import { Link } from 'gatsby';

import { Result } from 'antd';

import SEO from '../components/Seo/index';

import Container from '../components/Container/index';

const NotFoundPage = () => (
  <Container>
    <SEO title="404" />
    <Result
      status="error"
      title="404: Pagina no encontrada"
      subTitle="Lo sentimos, la página que visitaste no existe."
      extra={<Link to="/">Ir al Inicio</Link>}
    />
  </Container>
);

export default NotFoundPage;
