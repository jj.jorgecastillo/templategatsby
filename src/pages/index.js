import React from 'react';
import ReactFullpage from '@fullpage/react-fullpage';

import Container from '../components/Container';
import SocialMedia from '../components/SocialMedia';
import SEO from '../components/Seo/index';

import Home from '../components/Home';

import FormContact from '../components/FormContact';
import HomeSlides from '../components/HomeSlides';

const anchors = ['Inicio', 'Galería', 'Contacto', 'Redes'];

const IndexPage = () => (
  <Container>
    <SEO title="Inicio" />
    <ReactFullpage
      lockAnchors={false}
      navigationPosition="right"
      slidesNavigation
      slidesNavPosition="top"
      autoScrolling
      scrollBar={false}
      easing="easeInOutCubic"
      loopHorizontal
      continuousHorizontal={false}
      scrollHorizontally={false}
      interlockedSlides={false}
      dragAndMove={false}
      offsetSections={false}
      resetSliders={false}
      fadingEffect="sections"
      scrollOverflow
      touchSensitivity={15}
      bigSectionsDestination="null"
      keyboardScrolling
      animateAnchor
      recordHistory
      controlArrows={false}
      verticalCentered
      paddingBottom={4}
      fixedElements=".footer"
      responsiveWidth={0}
      responsiveHeight={0}
      responsiveSlides
      slidesNavigationTooltips={anchors}
      continuousVertical={false}
      paddingTop={4}
      anchors={anchors}
      navigation
      navigationTooltips={anchors}
      scrollingSpeed={1500}
      sectionsColor={['#0f3057', '#061629', '#0f3057', '#0f3057']}
      render={() => (
        <div className="Fullpage">
          <Home />
          <HomeSlides />
          <FormContact />
          <SocialMedia />
        </div>
      )}
    />
  </Container>
);

export default IndexPage;
