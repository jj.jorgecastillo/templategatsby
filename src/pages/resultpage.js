import React from 'react';
import ReactFullpage from '@fullpage/react-fullpage';

import SEO from '../components/Seo/index';

import Container from '../components/Container';

import ResultMessage from '../components/ResultMessage';

const ResultPage = () => (
  <Container>
    <SEO title="Resultado" />

    <ReactFullpage
      scrollOverflow
      sectionsColor={['#0f3057']}
      render={() => (
        <div className="Fullpage">
          <div className="section">
            <ResultMessage />
          </div>
        </div>
      )}
    />
  </Container>
);

export default ResultPage;
