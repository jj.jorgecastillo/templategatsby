/* eslint-disable object-curly-newline */
import { CarTwoTone, ShopTwoTone, StarTwoTone } from '@ant-design/icons';

// images service

import aire from './serviceImages/aire.jpg';
import alineacion from './serviceImages/alineacion.jpg';
import electroauto from './serviceImages/electroauto.jpg';
import electronica from './serviceImages/electronica.jpg';
import frenos from './serviceImages/frenos.jpg';
import lavado from './serviceImages/lavado.jpg';
import llantas from './serviceImages/llantas.jpg';
import lubricantes from './serviceImages/lubricantes.jpg';
import mecanica from './serviceImages/mecanica.jpg';
import pulido from './serviceImages/pulido.jpg';
import repuestos from './serviceImages/repuestos-min.jpg';
import sccaner from './serviceImages/sccaner.jpg';
import sincronizacion from './serviceImages/sincronizacion.jpg';
import suspension from './serviceImages/suspension.jpg';
import tapiceria from './serviceImages/tapiceria.jpg';

// images Promos
import promoTaxi from './promosImages/promoTaxi.jpg';
import promoAceite from './promosImages/promoAceite.jpg';
import PromoAire2 from './promosImages/04PromoAire2.jpg';
import PromoAire3 from './promosImages/04PromoAire3.jpg';

// import s6 from './septimaServices/shelbyTransparente.png';

const servicesData = [
  {
    id: 1,
    label: 'Aire acondicionado ',
    image: aire,
    description:
      ' Realizamos la instalación, la reparación y el mantenimiento de aire acondicionado para autos, camionetas, vans, mini buses, etc. Instalaciones de equipo A/C originales, de equipo A/C Alternativo, Rectificación del sistema de A/C, timón Cambiado Carga de R-134 A Ecológico. Brindamos garantía. ',
  },
  {
    id: 2,
    label: 'Electrónica Automotriz',
    image: electronica,
    description:
      'El motor de arranque, la energía de la bocina, el aire acondicionado, y las luces, entre otros accesorios dependen del sistema electrónico. El diagnóstico y la prevención de fallos son totalmente vitales para el buen funcionamiento del vehículo.',
  },
  {
    id: 3,
    label: 'Lavado',
    image: lavado,
    description:
      'Lavar tu auto con frecuencia es esencial para mantener la pintura como el primer día, Nosotros podemos ayudarte con la limpieza de tu vehículo. ',
  },
  {
    id: 4,
    label: 'Limpieza de tapicería',
    image: tapiceria,
    description:
      'Si la tapicería de tu auto necesita una limpieza profunda, en SHELBY te ofrecemos el mejor servicio de limpieza. Garantizamos el resultado final, ya que contamos con grandes profesionales y los mejores productos. ',
  },
  {
    id: 5,
    label: 'Lubricantes y filtración',
    image: lubricantes,
    description:
      'El cambio de aceite de motor es uno de los procesos más importantes en el mantenimiento. Lo más necesario que debes saber es que cada carro trabaja con un aceite en particular y según el tipo de aceite, su mantenimiento también puede ser diferente. Para ello Tenemos un amplio portafolio en lubricantes & filtros, además de contar con personal experto y altamente calificado. ',
  },
  {
    id: 6,
    label: 'Pulitura',
    image: pulido,
    description:
      'Es un sistema que te permite proteger y darle ese toque de brillo reluciente a tu automóvil. En caso de tener un carro con rayas o rasguños, la Pulitura 3M te funciona, ya que, elimina defectos en manchas, oxidación,  dando cuidado y protección al mantener el color ideal en tu carro. ',
  },
  {
    id: 7,
    label: 'Repuestos',
    image: repuestos,
    description:
      'Tenemos todas las opciones para tu vehículo. Extensa gama de repuestos multimarcas. Respaldo y garantía total.',
  },
  {
    id: 8,
    label: 'Alineación y Balanceo',
    image: alineacion,
    description:
      'La alineación y balanceo del carro son dos procedimientos que, aunque diferentes, suelen realizarse de manera simultánea. Ambos son fundamentales para alargar la vida útil de la suspensión, las llantas de tu carro y, sobre todo, para la seguridad de todos.',
  },
  {
    id: 9,
    label: 'Montallantas',
    image: llantas,
    description: 'Prestamos Servicio de Montallantas Efectivo y Garantizado.',
  },
  {
    id: 10,
    label: 'Diagnóstico Computarizado (Scanner)',
    image: sccaner,
    description:
      'El diagnóstico computarizado tiene como fin examinar el vehículo de posibles fallas en: la inyección electrónica, sensores de control de tracción, climatizador electrónico, airbags, caja automática y frenos ABS, entre otros.',
  },
  {
    id: 11,
    label: 'Electricidad Automotriz',
    image: electroauto,
    description:
      ' Las instalaciones eléctricas de un automóvil están formadas por la batería, el alternador, los fusibles y el arranque. Por eso, realizar un mantenimiento preventivo te ahorrará problemas y dinero.',
  },
  {
    id: 12,
    label: 'Mecánica General de Sistemas a Gasolina y Diésel',
    image: mecanica,
    description:
      'Nuestro servicio para motores a gasolina y diésel consiste en ver el funcionamiento y las partes que integran el vehículo para poder hacer un diagnóstico, dar mantenimiento y reparar las fallas más comunes en su sistema mecánico, eléctrico y electrónico.',
  },
  {
    id: 13,
    label: 'Frenos',
    image: frenos,
    description:
      'El sistema de frenos es uno de los principales elementos de seguridad activa del vehículo. Realizamos el mantenimiento y sustitución de sus distintos componentes ,  materiales de fricción como los discos y las pastillas ya que están sujetos a desgaste. ',
  },
  {
    id: 14,
    label: 'Sincronización',
    image: sincronizacion,
    description:
      'Básicamente la sincronización consiste en que coincida correctamente el funcionamiento de todas las piezas de los sistemas de encendido, distribución y alimentación del carro para recuperar su eficiencia original. Cuando hacemos la sincronización estaremos ayudando a que nuestro automóvil tenga un menor consumo de gasolina, un mejor desempeño del motor y un encendido más eficiente.',
  },
  {
    id: 15,
    label: 'Suspensión',
    image: suspension,
    description:
      ' La suavidad y el confort al conducir depende en gran parte del sistema de suspensión, cuando se tiene piezas con desgaste o deterioradas por golpes, esto se refleja en una mala alineación de dirección, desgas no uniforme de llantas y ruidos al rodar.',
  },
];

const PromotionsData = [
  {
    id: 0,
    src: sccaner,
    title: 'Revision de suspension y escaneo',
    price: 15,
    discount: null,
    note: 'Este precio solo es valida los Lunes y Martes',
    description: [
      { idPromo: 1, item: 'Se revisa el vehículo visualmente' },
      { idPromo: 2, item: 'Se realiza escaneo de DTC para abordado de diagnóstico.' },
    ],
  },
  {
    id: 1,
    src: promoTaxi,
    title: 'Lavado general taxis',
    price: 25,
    discount: 0,
    note: 'El encerado es de 6am a 9am y de 7pm a 10pm',
    description: [
      { idPromo: 0, item: 'Lavado General + Encerado' },
      { idPromo: 1, item: '10% descuento en mano de obra' },
      { idPromo: 2, item: '10% descuento en repuestos.' },
    ],
  },
  {
    id: 2,
    src: aire,
    title: 'Mantenimiento de de aire acondicionado automotriz',
    price: 65,
    discount: 0,
    note: 'El servicio se presta en vehículos que el sistema del aire este en funcionamiento',
    description: [
      { idPromo: 0, item: 'Recarga de gas.' },
      { idPromo: 1, item: 'Cambio de aceite del compresor.' },
    ],
  },
  {
    id: 3,
    src: PromoAire2,
    title: 'Mantenimiento de de aire acondicionado automotriz',
    price: 100,
    discount: 0,
    note: 'El servicio se presta en vehículos que el sistema del aire este en funcionamiento',
    description: [
      { idPromo: 0, item: 'Recarga de gas.' },
      { idPromo: 1, item: 'Cambio de aceite del compresor.' },
      { idPromo: 2, item: 'Lavado del condensador y evaporador interno.' },
    ],
  },
  {
    id: 4,
    src: PromoAire3,
    title: 'Mantenimiento de de aire acondicionado automotriz',
    price: 160,
    discount: 0,
    note: 'El servicio se presta en vehículos que el sistema del aire este en funcionamiento',
    description: [
      { idPromo: 0, item: 'Recarga de gas.' },
      { idPromo: 1, item: 'Cambio de aceite del compresor.' },
      { idPromo: 2, item: 'Lavado del condensador y evaporador interno..' },
      { idPromo: 3, item: 'Cambio del filtro del aceite y filtro de cabina.' },
    ],
  },
  {
    id: 5,
    src: promoAceite,
    title: 'Aceite y Filtros',
    price: 0,
    discount: 0,
    note: null,
    description: [
      { idPromo: 0, item: 'Se especifica que aceite es el más apropiado.' },
      { idPromo: 1, item: 'Con este servicio se le provee lavado gratis.' },
    ],
  },
  {
    id: 6,
    src: sincronizacion,
    title: 'Sincronizacion',
    price: 0,
    discount: 0,
    note: null,
    description: [
      {
        idPromo: 0,
        item:
          'Profundo chequeo de de todas las piezas en los sistemas de encendido, distribución y alimentación.',
      },
      { idPromo: 1, item: 'Con este servicio se le provee escaneo gratis' },
      { idPromo: 2, item: 'Con este servicio se le provee lavado gratis.' },
    ],
  },
  // {
  //   id: 4,
  //   src: llantas,
  //   title: 'Montaje de llantas',
  //   price: 0,
  //   discount: 0,
  //   note: null,
  //   description: [
  //     { idPromo: 0, item: 'Esperando detalles.' },
  //     { idPromo: 1, item: 'Al adquirir esta promo tendrás las válvulas gratis.' },
  //   ],
  // },
  // {
  //   id: 6,
  //   src: alineacion,
  //   title: 'Alineacion, Balanceo y Montaje',
  //   price: 0,
  //   discount: 25,
  //   note: null,
  //   description: [{ idPromo: 1, item: 'Esperando detalles.' }],
  // },
];

const menuData = [
  { key: '', icon: ShopTwoTone, text: 'INICIO' },
  { key: 'promotions', icon: StarTwoTone, text: 'PROMOCIONES' },
  { key: 'services', icon: CarTwoTone, text: 'SERVICIOS' },
];

// const dataImagesSeptimaServices = [{ key: 's6', imgService: s6 }];

const sideData = [
  { key: 1, label: 'Equipo' },
  { key: 2, label: 'Lobby' },
  { key: 3, label: 'Sala de descanso' },
  { key: 4, label: 'Baños' },
  { key: 5, label: 'Entrada' },
  { key: 6, label: 'Entrada' },
  { key: 7, label: 'Instalaciones' },
  { key: 8, label: 'Instalaciones' },
  { key: 9, label: 'Trabajando' },
  { key: 10, label: 'Producto' },
  { key: 11, label: 'Herramienta' },
  { key: 12, label: 'Herramienta' },
  { key: 13, label: 'Cliente Feliz' },
];

export { sideData, servicesData, menuData, PromotionsData };
